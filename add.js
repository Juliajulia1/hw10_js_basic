/*

Теоретичні питання
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
createElement() дозволяє створювати нові DOM-елементи appendChild та append додавати
2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.
const elementToRemove = document.getElementByClassName('navigation');
elementToRemove.remove();
3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?
before та after

Практичні завдання
 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.

 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
 Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.

 */
const link = document.createElement('a')
link.textContent = 'Learn More';
link.setAttribute('href', '#')
const paragraphInFooter = document.querySelector("footer p");
paragraphInFooter.after(link)


const newElem = document.createElement('select')
newElem.setAttribute('id', 'rating')
const featuresSection = document.querySelector('.features')
featuresSection.before(newElem)


for( let i = 4; i>0; i--){
    const optionElement = document.createElement('option')
    optionElement.textContent = `${i} Stars`
    optionElement.classList. add(`${i}`)
    newElem.appendChild(optionElement)
}


